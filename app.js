async function findIpAdress(){
    try {
     
    const request = await fetch("https://api.ipify.org/?format=json");
    const json = await request.json();
    let {ip} = json;
    const request2 = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,district,country,regionName,city`);
    const final = await request2.json();
     let {continent, country, regionName, city, district} = final;

    const div = document.createElement("div");
    div.textContent = 
    `Your continent: ${continent}; your country: ${country}; your region: ${regionName}; your city: ${city}; your district: ${getDistrict(district)}.`;
    document.body.append(div);
    } catch (e) {
        alert(e);
    }
    
    
}

let btn = document.querySelector("button");
btn.addEventListener("click", findIpAdress);



let getDistrict = (district) => {
   return district === "" ? "sorry, I don't know your district" : district;
}