/*
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.


Моє розуміння асинхронності таке: коли інтерпретатор коду доходить до блоку, 
який потребує певного часу на виконання, він не зупиняється і не чекає, коли блок виконається.
Натомість програма виконує подальші дії. А результат того блоку, який потребував часу на виконання, повертається пізніше.
Асинхронність в JS доступна завдяки промісам, колбекам, асинхронним функціям.   
 */